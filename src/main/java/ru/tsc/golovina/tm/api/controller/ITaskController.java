package ru.tsc.golovina.tm.api.controller;

import ru.tsc.golovina.tm.model.Task;

public interface ITaskController {

    void showTasks();

    void clearTasks();

    void createTask();

    void showTask(Task task);

    void showById();

    void showByName();

    void showByIndex();

    void updateByIndex();

    void updateById();

    void removeById();

    void removeByName();

    void removeByIndex();

    void startById();

    void startByIndex();

    void startByName();

    void finishById();

    void finishByIndex();

    void finishByName();

    void changeStatusById();

    void changeStatusByIndex();

    void changeStatusByName();

}
