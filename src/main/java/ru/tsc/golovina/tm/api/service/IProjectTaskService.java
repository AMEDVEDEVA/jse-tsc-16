package ru.tsc.golovina.tm.api.service;

import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findTaskByProjectId(String projectId);

    Task bindTaskById(String projectId, String taskId);

    Task unbindTaskById(String projectId, String taskId);

    Project removeById(String projectId);

    Project removeByIndex(Integer index);

    Project removeByName(String name);

}
