package ru.tsc.golovina.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showAbout();

    void showInfo();

    void showArgumentError();

    void exitSuccess();

    void exitError();
}
