package ru.tsc.golovina.tm.api.service;

import java.util.logging.Level;

public interface ILogService {

    void info(final String message);

    void debug(final String message);

    void command(final String message);

    void error(final Exception e);

}
