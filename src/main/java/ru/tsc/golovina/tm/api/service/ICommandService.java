package ru.tsc.golovina.tm.api.service;

import ru.tsc.golovina.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
