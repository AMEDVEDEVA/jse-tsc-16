package ru.tsc.golovina.tm.api.entity;

import ru.tsc.golovina.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}